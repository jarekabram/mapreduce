package com.sample.exercise;

import com.google.gson.Gson;
import com.sample.exercise.model.Finger;
import com.sample.exercise.model.FingerData;
import com.sample.exercise.model.FingerMetadata;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class SampleMapper extends Mapper<Object, Text, Text, FingerData> {

    private Text text = new Text();
    private FingerData fingerData = new FingerData();
    private FingerMetadata fingerMetadata = new FingerMetadata();
    Gson gson = new Gson();

    public void map(Object key, Text text, Context context) throws IOException, InterruptedException {
        // deserialize json to Finger object
        Finger finger = gson.fromJson(text.toString(), Finger.class);

        writeSpecificFinger(context, finger, "first", finger.getFeatures2D().getFirst());
        writeSpecificFinger(context, finger, "second", finger.getFeatures2D().getSecond());
        writeSpecificFinger(context, finger, "third", finger.getFeatures2D().getThird());
        writeSpecificFinger(context, finger, "fourth", finger.getFeatures2D().getFourth());
        writeSpecificFinger(context, finger, "fifth", finger.getFeatures2D().getFifth());
    }

    private void writeSpecificFinger(Context context, Finger finger, String fingerNumber, Double measuredValue) throws IOException, InterruptedException {

        // save series, side and finger number to Metadata object
        fingerMetadata.setSeries(finger.getSeries());
        fingerMetadata.setSide(finger.getSide());
        fingerMetadata.setFinger(fingerNumber);

        // save created Metadata object and value from each finger to FingerData
        fingerData.setFinger(fingerMetadata);
        fingerData.setValue(measuredValue);
        text.set(fingerData.getFinger().toString());

        context.write(text, fingerData);
    }

}
