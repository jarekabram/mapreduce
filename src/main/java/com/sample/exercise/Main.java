package com.sample.exercise;

import com.sample.exercise.model.FingerData;
import com.sample.exercise.model.Statistics;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.log4j.*;

public class Main {

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();
        Configuration conf = new Configuration();
        conf.set("fs.hdfs.impl",
                org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
        );
        conf.set("fs.file.impl",
                org.apache.hadoop.fs.LocalFileSystem.class.getName()
        );

        // set property hadoop.home.dir
        System.setProperty("hadoop.home.dir", "/usr/local/hadoop/");
        Job job = Job.getInstance(conf, "mapreduce");
        job.setJarByClass(Main.class);
        job.setMapperClass(SampleMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FingerData.class);
        job.setReducerClass(SampleReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Statistics.class);
        // input file must exist
        FileInputFormat.addInputPath(job, new Path(args[0]));
        // output file should not
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
