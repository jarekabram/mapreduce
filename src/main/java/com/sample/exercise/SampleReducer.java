package com.sample.exercise;

import com.sample.exercise.model.FingerData;
import com.sample.exercise.model.Statistics;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SampleReducer extends Reducer<Text, FingerData, Text, Statistics> {
    private List<Double> valuesList = new ArrayList<>();
    private Text text = new Text();
    private Statistics statistics = new Statistics();

    private void writeLog(String message) {
        System.out.println(message);
        try (PrintWriter out = new PrintWriter("/home/vagrant/mapreduce/result.log")) {
            out.println(text);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void reduce(Text key, Iterable<FingerData> fingerData, Context context) throws IOException, InterruptedException {
        double sum = 0;
        valuesList.clear();
        text.set(key.toString());
        statistics.setStandardDeviation(new Double(0.0));

        for (FingerData data : fingerData) {
            sum = sum + data.getValue();
            valuesList.add(data.getValue());
        }
        Collections.sort(valuesList);

        double sumOfSquares = 0;
        for (double value : valuesList) {
            sumOfSquares += (value - (sum / valuesList.size())) * (value - (sum / valuesList.size()));
        }

        statistics.setStandardDeviation(Math.sqrt(sumOfSquares / (valuesList.size() - 1)));
        context.write(text, statistics);
    }
}
