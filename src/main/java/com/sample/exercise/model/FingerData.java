package com.sample.exercise.model;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class FingerData implements Writable {
    private FingerMetadata finger = new FingerMetadata();
    private Double value;

    public FingerMetadata getFinger() { return finger; }
    public Double getValue() { return value; }
    public void setFinger(FingerMetadata finger) { this.finger = finger; }
    public void setValue(Double value) { this.value = value; }

    @Override
    public void write(DataOutput out) throws IOException {
        getFinger().write(out);
        out.writeDouble(getValue());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        setFinger(new FingerMetadata());
        getFinger().readFields(in);
        setValue(in.readDouble());
    }

    @Override
    public String toString() {
        return "Finger: \n" +
                "finger number: " + finger.getFinger() + "\n" +
                "finger series: " + finger.getSeries() + "\n" +
                "finger side: " + finger.getSide();
    }
}
