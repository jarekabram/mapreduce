package com.sample.exercise.model;

public class Finger {

    /* inner json element representing features2D */
    public class Features2D {
        private Double first;
        private Double second;
        private Double third;
        private Double fourth;
        private Double fifth;

        public Double getFirst() { return first; }
        public Double getSecond() { return second; }
        public Double getThird() { return third; }
        public Double getFourth() { return fourth; }
        public Double getFifth() { return fifth; }

        public void setFirst(Double first) { this.first = first; }
        public void setSecond(Double second) { this.second = second; }
        public void setThird(Double third) { this.third = third; }
        public void setFourth(Double fourth) { this.fourth = fourth; }
        public void setFifth(Double fifth) { this.fifth = fifth; }
    }

    /* inner json element representing features3d */
    public class Features3D {
        private PalmSection palmSection;
        private String defectsDistanceProportion;
        private String fingerLength;
        private NoFingerShapeFactor noFingerShapeFactor;
        private FingerVolume fingerVolume;

        public PalmSection getPalmSection() { return palmSection; }
        public String getDefectsDistanceProportion() { return defectsDistanceProportion; }
        public String getFingerLength() { return fingerLength; }
        public NoFingerShapeFactor getNoFingerShapeFactor() { return noFingerShapeFactor; }
        public FingerVolume getFingerVolume() { return fingerVolume; }
        public void setDefectsDistanceProportion(String defectsDistanceProportion) { this.defectsDistanceProportion = defectsDistanceProportion; }
        public void setPalmSection(PalmSection palmSection) { this.palmSection = palmSection; }
        public void setFingerLength(String fingerLength) { this.fingerLength = fingerLength; }
        public void setNoFingerShapeFactor(NoFingerShapeFactor noFingerShapeFactor) { this.noFingerShapeFactor = noFingerShapeFactor; }
        public void setFingerVolume(FingerVolume fingerVolume) { this.fingerVolume = fingerVolume; }

        /* inner json element representing features3D:palm-section */
        public class PalmSection {
            private Factor factor;

            public Factor getFactor() { return factor; }
            public void setFactor(Factor factor) { this.factor = factor; }

            /* inner json element representing features3D:palm-section:factor */
            public class Factor {
                private double value;

                public double getValue() { return value; }
                public void setValue(double value) { this.value = value; }
            }
        }

        /* inner json element representing features3D:no-finger-shape-factor */
        public class NoFingerShapeFactor {
            public PalmSection.Factor factor;

            public PalmSection.Factor getFactor() { return factor; }
            public void setFactor(PalmSection.Factor factor) { this.factor = factor; }
        }

        /* inner json element representing features3D:finger-volume */
        public class FingerVolume {
            private Double little;
            private Double index;
            private Double thumb;
            private Double middle;
            private Double ring;

            public Double getLittle() { return little; }
            public Double getIndex() { return index; }
            public Double getThumb() { return thumb; }
            public Double getMiddle() { return middle; }
            public Double getRing() { return ring; }

            public void setLittle(Double little) { this.little = little; }
            public void setIndex(Double index) { this.index = index; }
            public void setThumb(Double thumb) { this.thumb = thumb; }
            public void setMiddle(Double middle) { this.middle = middle; }
            public void setRing(Double ring) { this.ring = ring; }
        }
    }

    /* whole json element */
    private Long timestamp;
    private Features2D features2D;
    private String side;
    private Features3D features3D;
    private Integer series;
    private Integer sample;

    public Long getTimestamp() { return timestamp; }
    public Features2D getFeatures2D() { return features2D; }
    public String getSide() { return side; }
    public Features3D getFeatures3D() { return features3D; }
    public Integer getSeries() { return series; }
    public Integer getSample() { return sample; }

    public void setTimestamp(Long timestamp) { this.timestamp = timestamp; }
    public void setFeatures2D(Features2D features2D) { this.features2D = features2D; }
    public void setSide(String side) { this.side = side; }
    public void setFeatures3D(Features3D features3D) { this.features3D = features3D; }
    public void setSeries(Integer series) { this.series = series; }
    public void setSample(Integer sample) { this.sample = sample; }
}

