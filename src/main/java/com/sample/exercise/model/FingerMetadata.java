package com.sample.exercise.model;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import static org.apache.hadoop.io.WritableUtils.writeString;
import static org.apache.hadoop.io.WritableUtils.readString;

public class FingerMetadata implements Writable {
    private Integer series;
    private String side;
    private String finger;

    public Integer getSeries() { return series; }
    public String getSide() { return side; }
    public String getFinger() { return finger; }
    public void setSeries(Integer series) { this.series = series; }
    public void setSide(String side) { this.side = side; }
    public void setFinger(String finger) { this.finger = finger; }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(getSeries());
        writeString(out, side);
        out.writeUTF(finger);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        setSeries(in.readInt());
        setSide(readString(in));
        setFinger(in.readUTF());
    }

    @Override
    public String toString() {
        return  getSide() + "-" + getSeries() + "-" + getFinger();
    }
}