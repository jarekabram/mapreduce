package com.sample.exercise.model;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class Statistics implements Writable {
    private Double standardDeviation;

    public Double getStandardDeviation() { return standardDeviation; }

    public void setStandardDeviation(Double standardDeviation) { this.standardDeviation = standardDeviation; }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeDouble(getStandardDeviation());
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        setStandardDeviation(in.readDouble());
    }
    @Override
    public String toString() {
        return "stdDev(" + standardDeviation + ")";
    }
}
